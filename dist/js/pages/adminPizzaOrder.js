"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://42.115.221.44:8080/devcamp-pizza365/orders";

// biến CSDL đơn order
var gOrderDb = {
  orders: [],
  // method filter order
  filterOrder: function (paramFilterObj) {
    "use strict";
    var vFilterArr = [];
    vFilterArr = gOrderDb.orders.filter(function (e) {
      return (e.trangThai === paramFilterObj.status || paramFilterObj.status === "none")
        && (e.loaiPizza === paramFilterObj.pizzaType || paramFilterObj.pizzaType === "none")
    });
    return vFilterArr;
  }
};

// biến CSDL drinks
var gDrinkDb = {
  drinks: []
};

var gId = null;
var gOrderId = "";

// Khai báo biến hằng lưu trữ trạng thái form
const gFORM_MODE_NORMAL = 'Normal';
const gFORM_MODE_INSERT = 'Insert';
const gFORM_MODE_UPDATE = 'Update';
const gFORM_MODE_DELETE = 'Delete';

// Biến toàn cục lưu trạng thái form, mặc định NORMAL
var gFormMode = gFORM_MODE_NORMAL;

// biến mảng toàn cục chứa danh sách tên các thuộc tính
var gColName = [
  "orderId",
  "kichCo",
  "loaiPizza",
  "idLoaiNuocUong",
  "thanhTien",
  "hoTen",
  "soDienThoai",
  "trangThai",
  "chiTiet"
];

// biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gCOL_ORDERID = 0;
const gCOL_KICHCO = 1;
const gCOL_LOAIPIZZA = 2;
const gCOL_NUOCUONG = 3;
const gCOL_THANHTIEN = 4;
const gCOL_HOTEN = 5;
const gCOL_SDT = 6;
const gCOL_TRANGTHAI = 7;
const gCOL_CHITIET = 8;

// khai báo DataTable và mapping với Columns
var gOrderTable = $("#order-tabe").DataTable({
  searching: false,
  ordering: false,
  // paging: false,
  columns: [
    { data: gColName[gCOL_ORDERID] },
    { data: gColName[gCOL_KICHCO] },
    { data: gColName[gCOL_LOAIPIZZA] },
    { data: gColName[gCOL_NUOCUONG] },
    { data: gColName[gCOL_THANHTIEN] },
    { data: gColName[gCOL_HOTEN] },
    { data: gColName[gCOL_SDT] },
    { data: gColName[gCOL_TRANGTHAI] },
    { data: gColName[gCOL_CHITIET] }
  ],
  columnDefs: [
    {
      targets: gCOL_CHITIET,
      defaultContent:
        `<button class='btn btn-primary btn-sm edit'>
          <i class="far fa-edit"></i>
        </button>
        <button class='btn btn-danger btn-sm delete'>
          <i class='far fa-trash-alt'></i>
        </button> `
    },
    {
      targets: gCOL_NUOCUONG,
      render: function (dataId) {
        return renderDrink(dataId);
      }
    }
  ],
});

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
  onPageLoading();
});

// gán sự kiện click cho btn Lọc
$("#btn-filter").on("click", onBtnFilterClick);

// gán sự kiện click vào nút Edit của mỗi dòng
$(document).on("click", ".edit", function () {
  onClickEditBtn(this);
});

// gán sự kiện click cho btn Confirm trong Modal Detail (add+edit)
$("#modal-confirm-btn").on("click", function () {
  onBtnConfirmClick();
});

// gán sự kiện click cho btn Cancel trong Modal Detail (add+edit)
$("#modal-cancel-btn").on("click", function () {
  onBtnCancelClick();
});

// gán sự kiện click cho btn Delete của mỗi dòng
$(document).on("click", ".delete", function () {
  onClickDelBtn(this);
});

// gán sự kiện cho btn Confirm trong Modal Delete (delete)
$("#btn-confirm-delete").on("click", function () {
  onConfirmDeleteClick(this);
});

// reset dữ liệu khi ẩn modal Detail đi
$("#modal-detail").on("hidden.bs.modal", function () {
  resetData();
});

// gán sự kiện click vào btn Thêm
$("#add-btn").on("click", function () {
  onAddBtnClick();
});

// gán sự kiện onChange vào ô Select cỡ Pizza khi Insert
$("#select-combo").on("change", function () {
  onComboChange(this);
});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// Hàm xử lý sự kiện load trang
function onPageLoading() {
  "use strict";
  loadDrinks(); // load cho ô select trong Modal
  getAllOrders();
}

// Hàm xử lý sự kiện btn Filter click
function onBtnFilterClick() {
  "use strict";
  // B0: khai báo biến lưu trữ dữ liệu ở form Lọc
  var vFilterObj = {
    status: "",
    pizzaType: ""
  };
  // B1: thu thập dữ liệu
  vFilterObj.status = $("#select-status").find(":selected").val();
  vFilterObj.pizzaType = $("#select-pizza-type").find(":selected").val();
  // B2: validate dữ liệu
  // B3: xử lý sự kiện
  var vFilterArr = gOrderDb.filterOrder(vFilterObj);
  loadDataToTable(vFilterArr);
}

// Hàm xử lý sự kiện click nút Edit
function onClickEditBtn(paramBtn) {
  "use strict";
  // đổi trạng thái: update
  gFormMode = gFORM_MODE_UPDATE;
  // B0: khai báo biến lưu trữ dữ liệu
  var vRowDataObj = {};

  // B1: thu thập dữ liệu
  var vTableRow = $(paramBtn).parents("tr");
  vRowDataObj = gOrderTable.row(vTableRow).data();

  // B2: validate dữ liệu (bỏ qua)
  // B3: xử lý dữ liệu
  gId = vRowDataObj.id; // get id from obj data of row
  gOrderId = vRowDataObj.orderId; // get orderId from obj data of row

  // B4: xử lý hiển thị
  loadOrderToModal(vRowDataObj);
  $("#modal-detail").modal("show");
}

// Hàm xử lý sự kiện click nút Delete
function onClickDelBtn(paramBtn) {
  "use strict";
  // đổi trạng thái: Delete
  gFormMode === gFORM_MODE_DELETE;

  // B0: khai báo biến lưu trữ dữ liệu
  var vRowDataObj = {};

  // B1: thu thập dữ liệu
  var vTableRow = $(paramBtn).parents("tr");
  vRowDataObj = gOrderTable.row(vTableRow).data();

  // B2: validate dữ liệu (bỏ qua)
  // B3: xử lý dữ liệu
  gId = vRowDataObj.id; // get id from obj data of row

  // B4: xử lý hiển thị
  $("#modal-delete").modal("show");
}

// Hàm xử lý sự kiện click btn Confirm trong Modal Detail (add+edit)
function onBtnConfirmClick() {
  "use strict";
  // B0: khai báo biến lưu trữ dữ liệu
  var vNewOrder = {
    kichCo: "",
    duongKinh: -1,
    suon: "",
    salad: -1,
    loaiPizza: "",
    idVourcher: -1,
    idLoaiNuocUong: "",
    soLuongNuoc: -1,
    hoTen: "",
    thanhTien: -1,
    email: "",
    soDienThoai: "",
    diaChi: "",
    loiNhan: ""
  };

  // B1: thu thập dữ liệu 
  getDataOnForm(vNewOrder);

  // B2: validate dữ liệu
  if (gFormMode === gFORM_MODE_UPDATE) {
    // B3: call restAPI update dữ liệu
    updateConfirmedStatus(gId); // B4 bên trong hàm Update.

  } else { // chế độ Insert
    var vIsDataValid = validateData(vNewOrder);
    if (vIsDataValid) {
      // B3: xử lý dữ liệu
      // callAPI thông báo có đúng mã voucher hay không
      $.ajax({
        async: false,
        url: "http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/" + vNewOrder.idVourcher,
        type: "GET",
        dataType: "json",
        success: function (res) {
          toastr.success(`Đá áp thành công mã giảm giá ${res.phanTramGiamGia}%`);
        },
        error: function (ajaxContext) {
          // alert(ajaxContext.responseText);
          toastr.error("Mã giảm giá không hợp lệ");
        }
      });

      // callAPI creat new order
      $.ajax({
        url: gBASE_URL,
        type: "POST",
        dataType: "json",
        data: JSON.stringify(vNewOrder),
        contentType: "application/json;charset=UTF-8",
        success: function (resOrderObj) {
          // thông báo
          toastr.success('Đã tạo đơn thành công');
          // load lại trang
          getAllOrders();
        },
        error: function (ajaxContext) {
          alert(ajaxContext.responseText);
        }
      });

      // ẩn modal đi
      $("#modal-detail").modal("hide");
      resetData();
    }
  }
}

// Hàm xử lý sự kiện click btn Cancel trong Modal Detail (add+edit)
function onBtnCancelClick() {
  "use strict";
    // ẩn modal
    $("#modal-detail").modal("hide");
}

// Hàm xử lý sự kiện click btn Confirm trong Modal Delete (delete)
function onConfirmDeleteClick() {
  "use strict";
  // B1: thu thập dữ liệu (bỏ qua vì ID đã đc get khi click btn Chi tiết)
  // B2: validate dữ liệu (bỏ qua)
  // B3: call restAPI để Delete dữ liệu (del bằng ID)
  $.ajax({
    async: false,
    url: gBASE_URL + "/" + gId,
    type: "DELETE",
    contentType: "json",
    success: function (res) {
      Toast.fire({
        icon: 'success',
        title: 'Đã xóa thành công.'
      });
    },
    error: function (ajaxContext) {
      alert(ajaxContext.responseText);
    }
  });
  // B4: xử lý hiển thị
  $("#modal-delete").modal("hide");
  getAllOrders();
}

// Hàm xử lý sự kiện click nút Thêm mới
function onAddBtnClick() {
  "use strict";
  // đổi trạng thái: Insert
  gFormMode = gFORM_MODE_INSERT;
  // hiển thị Modal detail
  $("#modal-detail").modal("show");

  // disable các field phụ thuộc combo trong yêu cầu
  $("#input-duongkinh").attr("disabled", "disabled");
  $("#input-suon").attr("disabled", "disabled");
  $("#input-soluongnuoc").attr("disabled", "disabled");
  $("#input-salad").attr("disabled", "disabled");
  $("#input-payment").attr("disabled", "disabled");
  // hide các field không cần trong yêu cầu
  $("#input-orderid").parent().parent("div").hide();
  $("#input-ngaytaodon").parent().parent("div").hide();
  $("#input-ngaycapnhat").parent().parent("div").hide();
  $("#select-trangthai").parent().parent("div").hide();
  $("#input-discount").parent().parent("div").hide();
}

// Hàm xử lý sự kiện onChange ô select combo
function onComboChange(paramSelectCombo) {
  "use strict";
  // B1: thu thập dữ liệu
  // debugger;
  var vCombo = $(paramSelectCombo).val();
  // B2: validate
  if (vCombo == "") {
    toastr.error("Vui lòng chọn combo");
    return;
  } else {
    // B3: xử lý sự kiện
    // fill các thông tin đi theo combo vào 5 fields
    if (vCombo === "S") {
      $("#input-duongkinh").val('20');
      $("#input-suon").val(2);
      $("#input-soluongnuoc").val(2);
      $("#input-salad").val(200);
      $("#input-payment").val(150000);
    }
    if (vCombo === "M") {
      $("#input-duongkinh").val(25);
      $("#input-suon").val(4);
      $("#input-soluongnuoc").val(3);
      $("#input-salad").val(300);
      $("#input-payment").val(200000);
    }
    if (vCombo === "L") {
      $("#input-duongkinh").val(30);
      $("#input-suon").val(8);
      $("#input-soluongnuoc").val(4);
      $("#input-salad").val(500);
      $("#input-payment").val(250000);
    }
  }
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//Hàm get dữ liệu orders lấy từ server lên Table
function getAllOrders() {
  "use strict";
  $.ajax({
    url: gBASE_URL,
    type: "GET",
    dataType: "json",
    success: function (res) {
      // load dữ liệu ra Table
      gOrderDb.orders = res;
      loadDataToTable(gOrderDb.orders);
    },
    error: function (ajaxContext) {
      alert(ajaxContext.responseText);
    }
  });
}

// Hàm load dữu liệu ra Table
function loadDataToTable(paramOrderArr) {
  "use strict";
  gOrderTable.clear();
  gOrderTable.rows.add(paramOrderArr);
  gOrderTable.draw();
}

// Hàm load dữ liệu order lên Modal
function loadOrderToModal(paramOrderObj) {
  "use strict";
  var vDrinkName = renderDrink(paramOrderObj.idLoaiNuocUong);

  $("#input-orderid").attr("disabled", "disabled").val(paramOrderObj.orderId);
  $("#select-combo").val(paramOrderObj.kichCo);
  $("#select-combo").attr("disabled", "disabled");
  $("#input-duongkinh").attr("disabled", "disabled").val(paramOrderObj.duongKinh);
  $("#input-suon").attr("disabled", "disabled").val(paramOrderObj.suon);
  $("#select-drink option:selected").text(vDrinkName);
  $("#select-drink").attr("disabled", "disabled");
  $("#input-soluongnuoc").attr("disabled", "disabled").val(paramOrderObj.soLuongNuoc);
  $("#input-voucherid").attr("disabled", "disabled").val(paramOrderObj.idVourcher);
  $("#select-pizza").val(paramOrderObj.loaiPizza);
  $("#select-pizza").attr("disabled", "disabled");
  $("#input-salad").attr("disabled", "disabled").val(paramOrderObj.salad);
  $("#input-payment").attr("disabled", "disabled").val(paramOrderObj.thanhTien);
  $("#input-discount").attr("disabled", "disabled").val(paramOrderObj.giamGia);
  $("#input-fullname").attr("disabled", "disabled").val(paramOrderObj.hoTen);
  $("#input-email").attr("disabled", "disabled").val(paramOrderObj.email);
  $("#input-phonenumber").attr("disabled", "disabled").val(paramOrderObj.soDienThoai);
  $("#input-address").attr("disabled", "disabled").val(paramOrderObj.diaChi);
  $("#input-message").attr("disabled", "disabled").val(paramOrderObj.loiNhan);
  $("#select-trangthai").val(paramOrderObj.trangThai);
  $("#input-ngaytaodon").attr("disabled", "disabled").val(paramOrderObj.ngayTao);
  $("#input-ngaycapnhat").attr("disabled", "disabled").val(paramOrderObj.ngayCapNhat);
}

// Hàm load đồ uống vào mục select bằng call restAPI(ajax)
function loadDrinks() {
  "use strict";
  // gọi server lấy dữ liệu
  $.ajax({
    url: "http://42.115.221.44:8080/devcamp-pizza365/drinks",
    type: "GET",
    dataType: "json",
    success: function (res) {
      gDrinkDb.drinks = res; // lưu dữ liệu vào biến toàn cục

      var vSelectDrink = $("#select-drink");
      $("<option/>", {
        text: "--- Chọn đồ uống ---",
        value: ""
      }).appendTo(vSelectDrink);

      for (let bI = 0; bI < gDrinkDb.drinks.length; bI++) {
        var vOption = $("<option/>", {
          text: gDrinkDb.drinks[bI].tenNuocUong,
          value: gDrinkDb.drinks[bI].maNuocUong
        }).appendTo(vSelectDrink);
      }
    },
    error: function (ajaxContext) {
      alert(ajaxContext.responseText);
    }
  });
}

// Hàm thu thập dữ liệu khi thêm mới
function getDataOnForm(paramNewOrder) {
  "use strcit";
  paramNewOrder.kichCo = $("#select-combo").val();
  paramNewOrder.duongKinh = $("#input-duongkinh").val();
  paramNewOrder.suon = $("#input-suon").val();
  paramNewOrder.salad = $("#input-salad").val();
  paramNewOrder.loaiPizza = $("#select-pizza").val();
  paramNewOrder.idVourcher = $("#input-voucherid").val().trim();
  paramNewOrder.idLoaiNuocUong = $("#select-drink").val();
  paramNewOrder.soLuongNuoc = $("#input-soluongnuoc").val();
  paramNewOrder.hoTen = $("#input-fullname").val().trim();
  paramNewOrder.thanhTien = $("#input-payment").val();
  paramNewOrder.email = $("#input-email").val().trim();
  paramNewOrder.soDienThoai = $("#input-phonenumber").val().trim();
  paramNewOrder.diaChi = $("#input-address").val().trim();
  paramNewOrder.loiNhan = $("#input-message").val().trim();
}

// Hàm validate dữ liệu thêm mới
function validateData(paramNewOrder) {
  "use strict";
  if (paramNewOrder.kichCo === "") {
    toastr.warning("Chưa chọn combo");
    $("#select-combo").focus();
    return false;
  }
  if (paramNewOrder.loaiPizza === "") {
    toastr.warning("Chưa chọn pizza");
    $("#select-pizza").focus();
    return false;
  }
  if (paramNewOrder.idLoaiNuocUong === "") {
    toastr.warning("Chưa chọn đồ uống");
    $("#select-drink").focus();
    return false;
  }
  // if (paramNewOrder.idVourcher == "") {
  //   toastr.warning("Chưa điền voucher");
  //   $("#input-voucherid").focus();
  //   return false;
  // }
  if (isNaN(paramNewOrder.idVourcher)) {
    toastr.error("Voucher ID phải là một số");
    $("#input-voucherid").focus();
    return false;
  }
  if (paramNewOrder.hoTen === "") {
    toastr.warning("Chưa điền tên");
    $("#input-fullname").focus();
    return false;
  }
  // if (paramNewOrder.email === "") {
  //   toastr.warning("Chưa điền email");
  //   $("#input-email").focus();
  //   return false;
  // }
  if (!validateEmail(paramNewOrder.email)) {
    toastr.error("Email không hợp lệ");
    $("#input-email").focus();
    return false;
  }
  if (paramNewOrder.soDienThoai === "") {
    toastr.warning("Chưa điền SĐT");
    $("#input-phonenumber").focus();
    return false;
  }
  if (paramNewOrder.diaChi === "") {
    toastr.warning("Chưa điền địa chỉ");
    $("#input-address").focus();
    return false;
  }

  return true;
}

// Hàm validate email
function validateEmail(paramEmail) {
  var vEmailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  return vEmailReg.test(paramEmail);
}

// Hàm render maNuocUong sang tenNuocUong
function renderDrink(paramId) {
  "use strict";
  var vTenNuocUong = "";
  for (let bI = 0; bI < gDrinkDb.drinks.length; bI++) {
    if (paramId === gDrinkDb.drinks[bI].maNuocUong) {
      vTenNuocUong = gDrinkDb.drinks[bI].tenNuocUong;
    }
  }
  return vTenNuocUong;
}

// Hàm call server để update trạng thái Order trên server
function updateConfirmedStatus(paramId) {
  "use strict";
  // khởi tạo obj request confirmed status
  var vObjRequest = {
    trangThai: $("#select-trangthai").val()
  }
  // call API by ajax and handle event
  $.ajax({
    url: gBASE_URL + "/" + paramId,
    type: "PUT",
    contentType: "application/json;charset=UTF-8",
    data: JSON.stringify(vObjRequest),
    dataType: "json",
    success: function (res) {
      // B4: xử lý sự kiện front-end
      // thông báo
      if(vObjRequest.trangThai === "open") {
        Toast.fire({
          icon: 'success',
          title: 'Đơn đã mở.'
        });
      } else if(vObjRequest.trangThai === "confirmed") {
        Toast.fire({
          icon: 'success',
          title: 'Đơn đã xác nhận.'
        });
      } else {
        Toast.fire({
          icon: 'success',
          title: 'Đơn đã đóng.'
        });
      }
      // cập nhật lại trang Order Detail với dữ liệu đã update
      getAllOrders();
      // đóng modal
      $("#modal-detail").modal("hide");
    },
    error: function (paramError) {
      alert(paramError.responseText);
    }
  });
}

// Hàm reset dữ liệu
function resetData() {
  "use strict";
  // đổi chế độ về Normal
  gFormMode === gFORM_MODE_NORMAL;

  $("#select-combo").removeAttr("disabled");
  $("#select-pizza").removeAttr("disabled");
  $("#input-voucherid").removeAttr("disabled").val("");
  $("#input-fullname").removeAttr("disabled").val("");
  $("#input-email").removeAttr("disabled").val("");
  $("#input-phonenumber").removeAttr("disabled").val("");
  $("#input-address").removeAttr("disabled").val("");
  $("#input-message").removeAttr("disabled").val("");
  $("#select-drink").removeAttr("disabled").empty();

  // làm rỗng các trường
  $("#select-combo").val("");
  $("#input-duongkinh").val("");
  $("#input-suon").val("");
  $("#input-salad").val("");
  $("#input-soluongnuoc").val("");
  $("#input-payment").val("");
  $("#select-pizza").val("");

  // hiển thị lại đầy đủ các trường nếu có bị ẩn sau khi click Insert
  $("#input-orderid").parent().parent("div").show();
  $("#input-ngaytaodon").parent().parent("div").show();
  $("#input-ngaycapnhat").parent().parent("div").show();
  $("#select-trangthai").parent().parent("div").show();
  $("#input-discount").parent().parent("div").show();

  // cần thực hiện việc này để tránh ô select drinks bị trống
  loadDrinks();
}
